# Pomocowy oddolny przewodnik internetowy

## Opis

Domyślnie katalog danych jest przedstawiany na stronie: https://przewodnik.wspolneoparcie.org

To repozytorium przechowuje jedynie suche dane oraz kod źródłowy strony internetowej.

Zbiór miejsc pomocowych (w tych punktach uzyska się bezpłatnie pomoc):
- wyżywienie,
- nocleg,
- schronienie,
- opieka medyczna,
- konsultacje prawne,
- konsultacji psychologiczne

itp.

Obecnie zasięg punktów lub grup ograniczony jest do Wrocławia, czyli do grup/miejsc działających na terenie tego miasta. Jeżeli będą zasoby osobowe i chęci, nie ma przeciwskazań do rozszerzenia listy o inne regiony.

## Licencja użytkowania

Dotyczy kodu źródłowego strony internetowej. Dane wykorzystywane na niej są publicznie dostępne.

"Uznanie autorstwa-Użycie niekomercyjne-Na tych samych warunkach 4.0 Międzynarodowe (CC BY-NC-SA 4.0)"

https://creativecommons.org/licenses/by-nc-sa/4.0/deed.pl

Wolno: 

- kopiować i rozpowszechniać w dowolny sposób
- zmieniać, adaptować, tworzyć inne na podstawie tego

Jednocześnie:

- dzieło należy odpowiednio oznaczyć, podać link do licencji i wskazać jeśli zostały dokonane w nim zmiany
- nie wolno używać do celów komercyjnych
- wykonywać akcje powyżej w "Wolno" należy to robić używając tej samej licencji
- nie należy ograniczać używania dzieła przez prawo lub technologię

## Współtworzenie

Jest możliwość współtworzenia katalogu danych oraz kodu źródłowego strony.

Jedną z możliwości jest utworzenie konta na tym portalu, zgłosić wątek na stronie tego repozytorium w zakładce "Issues" lub zgłosić scalenie w zakładce "Pull Requests".

Inną z możliwości jest bezpośrednie skontaktowanie się z podmiotem opiekującym się tym repozytorium i stroną internetową: info@wspolneoparcie.org