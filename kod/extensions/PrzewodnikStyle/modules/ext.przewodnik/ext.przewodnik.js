


const sidebarButtonId = '#mw-sidebar-button';
$(sidebarButtonId).text($(sidebarButtonId).attr('title'));
$(sidebarButtonId).removeClass('mw-ui-icon').removeClass('mw-ui-icon-element');

const searchButtonClass = '.mw-ui-icon-wikimedia-search';
$(searchButtonClass).removeClass('mw-ui-icon').removeClass('mw-ui-icon-element');
