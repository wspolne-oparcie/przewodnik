<?php

namespace MediaWiki\Extension\PrzewodnikStyle;

use MediaWiki\Hook\BeforePageDisplayHook;
use OutputPage;
use Skin;

class PrzewodnikCustomResourcesHooks implements BeforePageDisplayHook {
    public function onBeforePageDisplay( $out, $skin ): void {
        $out->addModules( 'ext.przewodnik' );
    }
}