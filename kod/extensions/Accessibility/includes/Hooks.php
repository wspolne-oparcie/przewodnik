<?php

namespace MediaWiki\Extension\Accessibility;

use MediaWiki\Hook\BeforePageDisplayHook;
use OutputPage;
use Skin;

class Hooks implements BeforePageDisplayHook {
    public function onBeforePageDisplay( $out, $skin ): void {
        $out->addModules( 'ext.accessibility' );
    }
}