<?php

class SpecialAccessibilityPage extends SpecialPage {
	function __construct() {
		parent::__construct( 'Accessibility' );
	}

	function execute( $par ) {
        $request = $this->getRequest();
		$output = $this->getOutput();
		
		$this->setHeaders();
 		$output->addModules( 'ext.accessibility' );
		$output->setPageTitle(wfMessage('myextension'));
		
		$content = '<div id="accessibility-container"></div>';
		$output->addHTML($content);
	}
}
