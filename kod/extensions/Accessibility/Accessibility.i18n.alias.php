<?php
/**
 * Aliases for myextension
 *
 * @file
 * @ingroup Extensions
 */

$specialPageAliases = [];

/** Deutsch
 * @author Barto
 */
$specialPageAliases['pl'] = [
	'Accessibility' => [ 'Narzędzia dostępności' ],
];
