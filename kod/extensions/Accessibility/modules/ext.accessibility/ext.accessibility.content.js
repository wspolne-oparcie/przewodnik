/*
<input id="accessibility-font-default-id" value="accessibility-font-default" type="radio" name="accessibility-radios-font"/>
<label for="accessibility-font-default-id">Czcionka domyślna</label><br/>
<input id="accessibility-font-dyslexic-id" value="accessibility-font-dyslexic" type="radio" name="accessibility-radios-font"/>
<label for="accessibility-font-dyslexic-id">Czcionka dyslektyczna</label><br/>
*/

const commonStyleClass = {
    button: "mw-ui-button"
};

const inputsId = {
    fontDefault: "accessibility-font-default-id",
    fontDyslexic: "accessibility-font-dyslexic-id",
    fontSizeInc: "accessibility-font-size-inc-id",
    fontSizeDec: "accessibility-font-size-dec-id",
}
const inputsGroups = {
    fontFamily: "accessibility-radios-font",
    fontSizeChange: "accessibility-buttons-font-size"
}
const inputsValues = {
    fontDyslexic: "accessibility-font-dyslexic",
    fontDefault: "accessibility-font-default"
}

const breakElement = () => document.createElement("br");

const getInput = ({id, value, name, type, className}) => {
    const input = document.createElement('input');
    input.id = id;
    input.value = value;
    input.name = name;
    input.type = type;
    input.classList.add(className);
    return input;
};
const getLabel = ({forId, innerContent}) => {
    const label = document.createElement('label');
    label.htmlFor = forId;
    label.innerHTML = innerContent;
    return label;
};

const getFontFamilySelectors = () => {
    const elementType = "radio";
    
    const inputDefault = getInput({
        id: inputsId.fontDefault,
        value: "accessibility-font-default",
        type: elementType,
        name: inputsGroups.fontFamily
    });
    const inputDyslexic = getInput({
        id: inputsId.fontDyslexic,
        value: "accessibility-font-dyslexic",
        type: elementType,
        name: inputsGroups.fontFamily
    });
    
    const labelForDefault = getLabel({
        forId: inputDefault.id,
        innerContent: "Czcionka domyślna"
    });
    const labelForDyslexic = getLabel({
        forId: inputDyslexic.id,
        innerContent: "Czcionka dyslektyczna"
    });
    
    const container = document.createElement("div");
    container.appendChild(inputDefault);
    container.appendChild(labelForDefault);
    container.appendChild(breakElement());
    container.appendChild(inputDyslexic);
    container.appendChild(labelForDyslexic);
    container.appendChild(breakElement());
    
    return container;
};

const getFontSizeSelectors = () => {
    const elementType = "button";
    
    const inputIncreaseFontSize = getInput({
        id: inputsId.fontSizeInc,
        value: "Zwiększenie czcionki",
        type: elementType,
        className: commonStyleClass.button,
        name: inputsGroups.fontSizeChange
    });
    const inputDecreaseFontSize = getInput({
        id: inputsId.fontSizeDec,
        value: "Zmniejszenie czcionki",
        type: elementType,
        className: commonStyleClass.button,
        name: inputsGroups.fontSizeChange
    });
    
    const container = document.createElement("div");
    container.appendChild(inputIncreaseFontSize);
    container.appendChild(inputDecreaseFontSize);
    
    return container;
};

const fillSpecialPageIfVisible = () => {
    const pageBody = document.getElementById("accessibility-container");
    if(pageBody){
        const selectorsFontFamily = getFontFamilySelectors();
        pageBody.appendChild(selectorsFontFamily);
        
        pageBody.appendChild(breakElement());
        pageBody.appendChild(breakElement());
        
        const selectorsFontSize = getFontSizeSelectors();
        pageBody.appendChild(selectorsFontSize);
    }
};

module.exports = {
    fillSpecialPageIfVisible,
    inputsGroups,
    inputsId,
    inputsValues
};