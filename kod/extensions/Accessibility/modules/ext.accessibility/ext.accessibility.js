/*
libs:
jquery
https://doc.wikimedia.org/mediawiki-core/master/js/#!/api/mw.cookie
*/

const {inputsId, inputsGroups, inputsValues, fillSpecialPageIfVisible} = require("./ext.accessibility.content.js");

const settingsRange = {
    fontSizeStepMax: 3,
}
const inputsQuery = {
    fontFamily: 'input[name="'+inputsGroups.fontFamily+'"]',
    fontSize: 'input[name="'+inputsGroups.fontSizeChange+'"]'
};
const accessibilityCookieKey = 'accessibility-tool';
const cookieOptions = {
    sameSite: "None",
    secure: true
};
const styleClasses = {
    fontDyslexic: "accessibility-font-dyslexic",
    fontSizeLarge: "accessibility-font-size-large",
    fontSizeXLarge: "accessibility-font-size-xlarge",
    fontSizeXXLarge: "accessibility-font-size-xxlarge",
}

const getCookieValue = () => {
    const cookie = mw.cookie.get(accessibilityCookieKey);
    return JSON.parse(!cookie ? "{}" : cookie);
};
const setCookieValue = ({dyslexicEnabled, fontSizeStep}) => {
    const oldAccessibilityOptions = getCookieValue(accessibilityCookieKey);
    const newAccessibilityOptions = {
        dyslexicEnabled: dyslexicEnabled == null ? oldAccessibilityOptions.dyslexicEnabled : dyslexicEnabled,
        fontSizeStep: fontSizeStep == null ? oldAccessibilityOptions.fontSizeStep : fontSizeStep
    };
    mw.cookie.set(accessibilityCookieKey, JSON.stringify(newAccessibilityOptions), cookieOptions);  
};

const setGlobalStyle = (styleClass, enabled) =>{
    if(enabled){
        $('body').addClass(styleClass);
    }else{
        $('body').removeClass(styleClass);
    }
}

const setFontDyslexic = (enabled)=>{
    setGlobalStyle(styleClasses.fontDyslexic, enabled);
    setCookieValue({dyslexicEnabled: enabled});
};
const onFontFamilyClick = ()=>{
    const checkedValue = $(inputsQuery.fontFamily+':checked').val();
    setFontDyslexic(checkedValue === inputsValues.fontDyslexic);
};

const setFontSize = (fontSizeStep) => {
    setGlobalStyle(styleClasses.fontSizeLarge, fontSizeStep === 1);
    setGlobalStyle(styleClasses.fontSizeXLarge, fontSizeStep === 2);
    setGlobalStyle(styleClasses.fontSizeXXLarge, fontSizeStep === 3);
};
const changeFontSize = (increase) => {
    const savedOptions = getCookieValue();
    let fontSizeStep = savedOptions.fontSizeStep;
    if(fontSizeStep==null){
        fontSizeStep = 0;
    }
    if(increase){
        if(fontSizeStep < settingsRange.fontSizeStepMax){
            fontSizeStep++;
        }
    }else{
        if(fontSizeStep > 0){
            fontSizeStep--;
        }
    }
    setCookieValue({fontSizeStep: fontSizeStep});
    setFontSize(fontSizeStep);
};
const onFontSizeChangeClick = (eventObject) => {
    changeFontSize(eventObject.currentTarget.id === inputsId.fontSizeInc);
}

const setStartupInputsValues = ({dyslexicEnabled, fontSizeStep}) => {
    if(dyslexicEnabled){
        $('#'+inputsId.fontDyslexic).attr('checked', 'true');
    }else{
        $('#'+inputsId.fontDefault).attr('checked', 'true');
    }
}

// main

fillSpecialPageIfVisible();
$(inputsQuery.fontFamily).click(onFontFamilyClick);
$(inputsQuery.fontSize).click(onFontSizeChangeClick);

const accessibilityOptions = getCookieValue(accessibilityCookieKey);
console.log(accessibilityOptions);

if(accessibilityOptions){
    setFontDyslexic(accessibilityOptions.dyslexicEnabled);    
    setFontSize(accessibilityOptions.fontSizeStep);
    setStartupInputsValues(accessibilityOptions);
}else{
    setFontDyslexic(false);    
    setStartupInputsValues({});
}



