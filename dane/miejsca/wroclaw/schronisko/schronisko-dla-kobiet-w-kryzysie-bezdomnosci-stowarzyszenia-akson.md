# Schronisko dla kobiet w kryzysie bezdomności “Dom dla Kobiet” Stowarzyszenia Akson

## informacje

Adres: Gliniana 28/30 50-525 Wrocław

Najblisze przystanki: Joannitów , Gajowa , Dyrekcyjna

Czynne: całodobowo

Kontakt telefoniczny: +48 713 671 415

Kontakt internetowy: akson.zmch@wp.pl

Strona internetowa: https://www.akson.org.pl/dom-dla-bezdomnych-kobiet-matek-z-dziecmi-i-kobiet-w-ciazy/

## opis

Wymagane skierowanie z Miejskiego Ośrodka Pomocy Społecznej Wrocław (przejście do strony MOPS Wrocław).
Interwencyjnie miejsce dostępne dla 3-6 osób samodzielnych.

Mogą skorzystać kobiety w kryzysie bezdomności, matki z dziećmi i kobiety w ciąży posiadające ostatnie stałe zameldowanie na terenie Gminy Wrocław. Do Domu dla Kobiet przyjmowane są w pierwszej kolejności osoby lub rodziny, którym kończy się pobyt w ośrodkach interwencyjnych prowadzonych przez Stowarzyszenie
Udzielana jest całodobowa pomoc w formie schronienia (na okres do 6 miesięcy), we wszystkie dni tygodnia, również w niedziele i święta. Osoby mieszkające ponoszą częściową odpłatność za zużyte media.
Od poniedziałku do piątku udzielana jest bezpłatnej pomoc specjalistyczna (dla osób mieszkających i osób nie mieszkających w ośrodku):

- pomoc psychologiczną
- pomoc prawną
- pomoc pedagogiczną
- pomoc terapeutyczną
- pomoc socjalną