# Łaźnia Caritas Archidiecezji Wrocławskiej

## informacje

Adres: Katedralna 7, 50-328 Wrocław

Najblisze przystanki: Plac Bema , Hala Targowa , Katedra

Czynne: poniedziałek, środa, piątek, od 6:00 do 14:00

Kontakt telefoniczny: +48 713 271 316

Kontakt internetowy: abnys@caritas.wroclaw

Strona internetowa: http://wroclaw.caritas.pl/placowki/laznia/

## opis

Do łaźni, można przynosić:

- koszulki
- spodnie
- bieliznę
- buty
- kurtki, czapki i szaliki
- środki higieniczne: maszynki do golenia, pianki do golenia, grzebienie, itp.