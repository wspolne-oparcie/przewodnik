# Schronisko św. Brata Alberta dla kobiet w kryzysie bezdomności

## informacje

Adres: Strzegomska 9 (wejście od Stacyjnej) 53-611 Wrocław

Najblisze przystanki: Zachodnia (stacja kolejowa) , Śrubowa , Wrocławski Park Przemysłowy

Czynne: całodobowo

Kontakt telefoniczny: +48 713 554 466

Kontakt internetowy: wroclaw-strzegomska@tpba.pl

Strona internetowa: https://www.bratalbert.wroclaw.pl/STRZEGOMSKA

## opis

Wymagane skierowanie z Miejskiego Ośrodka Pomocy Społecznej Wrocław (przejście do strony MOPS Wrocław).

Schronisko mieści się we Wrocławiu przy ul. Strzegomskiej 9 (wejście od Stacyjnej) — w bliskim sąsiedztwie stacji kolejowej Wrocław-Mikołajów. Budynek posiada dwa skrzydła, łącznie mieszka w nim zwykle około 40–60 kobiet w różnym wieku. 

W schronisku przebywają osoby w kryzysie bezdomności, bez opieki ze strony rodziny, z wieloma poważnymi chorobami.
Schronisko otwarte jest całą dobę, udziela wsparcia socjalno-bytowego: w zakresie noclegów, posiłków, odzieży. W czasie pandemii osoby z interwencji przyjmuje się do izolatorium na 10 dni kwarantanny, następnie — do schroniska. Osoby posiadające aktualne testy przyjmowane są bezpośrednio do schroniska.