# Centrum Wsparcia – schronisko dla osób w kryzysie bezdomności Stowarzyszenia Pomocy Wzajemnej

## informacje

Adres: Lelewela 23, 53-505 Wrocław

Adres jest adresem biura, którego lokal ma numer 2.

Najbliższe przystanki: Plac Legionów , Kolejowa , Piłsudskiego

Czynne / Dostępne: całodobowo, biuro w godzinach 9:00 do 18.00

Kontakt telefoniczny: +48 713 412 897 , +48 713 555 643

Kontakt internetowy: info@spw.wroclaw.pl

Strona internetowa: https://spw.wroclaw.pl/?page_id=1638 

## opis

Wymagane skierowanie z Miejskiego Ośrodka Pomocy Społecznej Wrocław.

Wymagana rozmowa z osobą kierowniczą placówki.

To zespół mieszkań dla osób starszych, w kryzysie bezdomności lub osób zagrożonych bezdomnością. Dom znajdują tu osoby będące w trudnej sytuacji życiowej wynikającej z wieku, niepełnosprawności, przewlekłej choroby, a w szczególności z zaburzeń psychicznych. Osoby potrzebujące wsparcia w funkcjonowaniu w codziennym życiu, ale nie wymagające usług w zakresie świadczonym przez jednostkę całodobowej opieki. Pobyt w mieszkaniu chronionym jest miejscem doświadczania samodzielności i wychodzenia z bezdomności.

Osoby mieszkające w Centrum Wsparcia żyją w pokojach jedno lub dwuosobowych (Centrum Wsparcia posiada pokoje dla 36 mieszkańców), mają do dyspozycji wyposażone aneksy kuchenne, łazienki i pralnie, dostęp do świetlicy z telewizorem, i dostęp do internetu. Mieszkańcy samodzielnie przygotowują sobie posiłki, gospodarują czasem wolnym, nawiązują kontakty społeczne. Wsparcia w codziennym funkcjonowaniu i rozwiązywaniu problemów życiowych udziela mieszkańcom personel – opiekunki, pracownik socjalny, psycholog, prawnik. Mieszkańcy mają okazję uczestniczyć w organizowanych na terenie Centrum Wsparcia zajęciach aktywizacyjnych – wieczorów spotkań ze sztuką, prelekcji, warsztatów, lektoratów języków obcych. Mieszkające tam osoby są na bieżąco informowane zachęcane do uczestnictwa w różnego rodzaju ofercie wydarzeń kulturalnych, sportowych, rozrywkowych i innych organizowanych poza Centrum Wsparcia. Część mieszkańców aktywnie udziela się na zewnątrz – bierze udział w projektach innych organizacji pozarządowych, działa w ramach Uniwersytetu Trzeciego Wieku, uczestniczy w imprezach organizowanych na terenie miasta. 