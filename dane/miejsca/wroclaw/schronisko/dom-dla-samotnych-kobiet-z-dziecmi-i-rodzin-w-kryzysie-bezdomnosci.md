# Dom dla samotnych kobiet z dziećmi i rodzin w kryzysie bezdomności Stowarzyszenia Osób Bezdomnych Nadzieja

## informacje

Adres: Pieszycka 32, 50-537 Wrocław

Najbliższe przystanki: Świeradowska , Morwowa , Gaj

Czynne / Dostępne: do umówienia telefonicznie

Kontakt telefoniczny: +48 713 925 912

Kontakt internetowy: nadziejastowarzyszenie@gmail.com

Strona internetowa: https://www.codalej.info/listings/11216/ 

## opis

Podany adres jest adresem biura Stowarzyszenia Osób Bezdomnych Nadzieja.

Wymagane skierowanie z Miejskiego Ośrodka Pomocy Społecznej Wrocław.

Stowarzyszenie zajmuje się:

- administrowanie całodobowym lokalem zastępczym dla osób bezdomnych,
- przyjmowanie nowych osób, rodzin,
- pozyskiwanie odzieży, produktów higienicznych, żywności od sponsorów,
- egzekwowanie zasad wynikających z regulaminu,
- organizowanie pogadanek,
- rozwiązywanie sytuacji konfliktowych występujących między rodzinami,
- zachęcanie osób o udziału w szkoleniach organizowanych przez MOPS i Urząd Pracy, pomagające w zdobyciu pracy (aktywne poszukiwanie pracy, pisanie CV, autoprezentacja)
- prowadzenie kursów pomocniczych w uzyskaniu zawodu (kursy obsługi sprzętów biurowych, kurs komputerowy) prowadzone bezpłatnie przez Biuro
- pomoc w uzyskaniu należnych świadczeń pomocy społecznej – kontakt MOPS,
- współpracę z innymi instytucjami udzielającymi pomocy osobom w trudnej sytuacji życiowej,
- opieka nad małoletnimi, przebywającymi w Ośrodku – prowadzenie dozoru w uczęszczaniu do szkoły, kontakt z pedagogami.

Osobom znajdującym się w sytuacjach kryzysowych zapewniane są:

- zapewnienie lokalu, wyżywienia, niezbędnej odzieży oraz podstawowych środków higieny,
- pomoc w powrocie do stałego miejsca zamieszkania,
- pomoc w uzyskaniu należnych świadczeń pomocy społecznej- kontakt z MOPS
- wspieranie procesu usamodzielniania się, wychodzenia z bezdomności, samopomocy (np. pomoc w znalezieniu mieszkania, poszukiwaniu pracy) – pisanie wniosków o pracę, przyznanie mieszkania itp., pozyskiwanie stałych pracodawców dla podopiecznych,
- zachęcanie do podnoszenia kwalifikacji zawodowych beneficjentów, mających ułatwić im samodzielne podjecie pracy (kurs języka ang., obsługi komputera itp. w ramach bezpłatnych kursów organizowanych przez MOPS i Urzędu Pracy)