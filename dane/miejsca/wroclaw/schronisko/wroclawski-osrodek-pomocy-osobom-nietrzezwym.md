# Wrocławski Ośrodek Pomocy Osobom Nietrzeźwym - WrOPON Stowarzyszenia Pomocy Wzajemnej

## informacje

Adres: Sokolnicza 14-18, 53-676 Wrocław

Najblisze przystanki: Plac Jana Pawła 2 , Plac Solidarności , Dworzec Świebodzki

Czynne: całodobowe

Kontakt telefoniczny: +48 713 542 037

Strona internetowa: https://spw.wroclaw.pl/?page_id=1654

## opis

We WrOPON są 48 łóżka w 11 salach przygotowanych specjalnie z myślą o bezpieczeństwie i komforcie trzeźwienia osób doprowadzonych do wytrzeźwienia. Nietrzeźwym proponowane są czystą piżamę oraz przygotowane łóżko z pościelą, oferowany jest również stały dostęp do wody źródlanej, kawy, herbaty. Dodatkowo dla osób bezdomnych jest posiłek.

Każda osoba po wytrzeźwieniu może skorzystać z sanitariatów, ręcznika i mydła. Osoba opuszczająca Dział Izby Wytrzeźwień zachęcana jest do skorzystania z specjalistycznej pomocy psychologa.