#  Dom Socjalny dla Mężczyzn Stowarzyszenia Pomocy Ludzie Ludziom

## informacje

Adres: Obornicka 58a, 51-114 Wrocław

Adres jest adresem biura.

Najbliższe przystanki: Bałtycka , Różanka , Kamieńskiego

Czynne / Dostępne: w godzinach 9:00 do 17:00

Kontakt telefoniczny: +48 713 290 806

Kontakt internetowy: Brak

Strona internetowa: http://ludzieludziom.pl/obszary-dzialania/dom-socjalny-dla-mezczyzn/ 

## opis

Stowarzyszenie jako jedna z niewielu na terenie Dolnego Śląska, posiada wieloletnie doświadczenie w pracy z bezdomnymi osobami opuszczającymi jednostki penitencjarne.

Zakres realizowanego w Domu Socjalnym dla Mężczyzn wsparcia stanowi zapewnienie schronienia dorosłym mężczyznom z terenu Wrocławia, w szczególności opuszczającym jednostki penitencjarne (zakłady karne i areszty śledcze), zakłady poprawcze, placówki opiekuńczo-wychowawcze i/lub resocjalizacyjne, którzy ze względu na trudną sytuację życiową (bezdomność) potrzebują wsparcia w funkcjonowaniu w życiu codziennym, a w momencie przyjęcia do Domu Socjalnego pozbawione są miejsca zamieszkania i nie posiadają wystarczających środków i/lub możliwości, by zabezpieczyć własne potrzeby życiowe, ale nie wymagają usług w zakresie świadczonym przez placówki opieki całodobowej. 