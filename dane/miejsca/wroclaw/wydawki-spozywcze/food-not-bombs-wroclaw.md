# Food Not Bombs Wrocław, Jedzenie Zamiast Bomb

## informacje

Adres: plac Powstańców Wielkopolskich, 50-232 Wrocław

Najbliższe przystanki: Dworzec Nadodrze , Plac Staszica , Nowowiejska

Czynne / Dostępne: w każdą sobotę, od godziny 10:00 trwa przygotowanie posiłku, od godziny 15:00 trwa wydawka posiłku

Kontakt internetowy: fnb@wolnywroclaw.pl

Strona internetowa: https://www.facebook.com/fnbwro 

## opis

Jedzenie Zamiast Bomb to ogólnoświatowa, anarchistyczna i wegańska inicjatywa rozdawania darmowych posiłków potrzebującym osobom. Posiłki są wydawane w każdą sobotę od godziny 15:00 na przystanku tramwajowo-autobusowym Dworcu Nadodrze (plac Powstańców Wielkopolskich) we Wrocławiu, a przygotowywane w tym samym dniu od godziny 10:00 w Centrum Reanimacji Kultury na Jagiellończyka 10c/d.

Produkty wykorzystywane w ramach akcji Food not Bombs nie są kupowane, ale odzyskiwane z żywności marnotrawionej – wyrzuconej czy niepotrzebnej.