# Wydawka Zupy na Wolności

## informacje

Adres: Plac Wolności, 50-071 Wrocław Stare Miasto

Najblisze przystanki: Zamkowa , Plac Orląt Lwowskich , Renoma

Czynne: w każdą niedziele przygotowanie posiłku od 13:30 do 16:30, wydawka od godziny 17:00

Kontakt internetowy: zupanawolnosciwroclaw@gmail.com

Strona internetowa: https://www.facebook.com/zupanawolnosci/

## opis

W każdą niedzielę między godzinami 13:30 a 16:30 gotowana jest zupa, żeby o godzinie 17:00 wspólnie zjeść ją z osobami w kryzysie bezdomności i osobami potrzebującymi na wrocławskim Placu Wolności. Są też między innymi ciasta, kawa i herbata. Dodatkowo, grupa stara się wspomagać zbiórkami ubrań, butów, środków czystości, materiałów opatrunkowych i produktów pierwszej potrzeby, a także organizując pomoc medyczną. Przede wszystkim jednak osoby wolontariackie rozmawiają, słuchają, a często również bardzo dobrze się bawią słuchając muzyki na żywo.

Zupa na Wolności jest kolektywem, grupą osób, którzy chcą dzielić się swoim czasem. Historia kolektywu jest związana z czwórką przyjaciół, którzy założyli Zupę we Wrocławiu na podobieństwo krakowskiego wzorca (Zupy na Plantach). Idee, które im przyświecały to przełamywanie barier, oswajanie bezdomności, dzielenie się z potrzebującymi i hasło “nigdy nie będziesz szła sama/szedł sam” („szło samo” dodaje os. red.). Dzisiaj inicjatywa to prawie 500 wolontariuszy oraz 40 stałych aktywistów koordynujących przeróżne projekty.