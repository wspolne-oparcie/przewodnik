# Wydawka Fundacji Z Głodu i pragnienia na rzecz kultury przetrwania

## informacje

Adres: Centrum Reanimacji Kultury przy Jagiellończyka 10cd, 50-240 Wrocław Nadodrze

Najblisze przystanki: Paulińska , Plac Staszica , Dworzec Nadodrze

Czynne: w każdy czwartek od godziny około 11:00 do godziny około 12:30

Strona internetowa: grupa na portalu Facebook https://www.facebook.com/groups/877170709416376

## opis

Cotygodniowa wydawka produktów spożywczych, najczęściej warzyw i owoców, zebranych z lokalnych targów lub dostarczonych bezpłatnie bezpośrednio od podmiotów sprzedających takie produkty. Korzystać może każda osoba, która przyjdzie. Zazwyczaj panuje zasada pierwszeństwa ustawienia się w kolejce, natomiast produktów do wzięcia jest ogromna ilość. 