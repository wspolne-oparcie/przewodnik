# Wydawka Fundacji WeźPomóż

## informacje

Adres: Poznańskia 58, 53-630 Wrocław Szczepin

Najblisze przystanki: Litomska (ZUS) , Poznańska , Szczepin

Czynne: (od 19 września 2022) poniedziałek 16:00-19:00, wtorek 12:00-16:00, środa 12:00-16:00, czwartek 12:00-16:00, piątek 16:00-19:00

Kontakt telefoniczny: +48 508 101 016

Kontakt internetowy: kontakt@wezpomoz.pl

Strona internetowa: https://wezpomoz.pl/

## opis

Pozyskują jedzenie z różnych supermarketów dzięki współpracy z Bankiem Żywności we Wrocławiu i wydają zebrane jedzenie potrzebującym osobom - tygodniowo nawet 4 tony żywności.

Do wydawki należy być osobą zweryfikowaną, zarejestrowaną w systemie Fundacji. Od 19 września 2022 roku weryfikacja dla nowych osób będzie przeprowadzana w poniedziałki i środy od godz. 15:00 do 18:00 w siedzibie Fundacji pod adresem Poznańskia 58.