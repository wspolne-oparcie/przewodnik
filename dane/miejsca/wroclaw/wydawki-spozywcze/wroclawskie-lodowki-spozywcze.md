# Wrocławskie Lodówki Spożywcze

## Lodówki bez opieki organizacji pozarządowej

- na lokalnym targowisku przy ul. Niedźwiedziej

## Lodówki Fundacji WeźPomóż

https://wezpomoz.pl/lodowka-spoleczna/

- Wojanowska 20, 54-063 Wrocław
- Pilczycka 47, 54-144 Wrocław
- Kolista 14K, 54-152 Wrocław
- Osobowicka 129, 51-004 Wrocław
- Strzegomska 49, 53-611 Wrocław
- Grabiszyńska 184, 53-235 Wrocław
- Zachodnia 1, 53-643 Wrocław
- Grabiszyńska 56, 53-504 Wrocław
- Kazimierza Wielkiego 37, 50-438 Wrocław
- Sportowa 2, 51-149 Wrocław
- Jedności Narodowej 187a, 50-303 Wrocław
- Namysłowska 8, 50-302 Wrocław
- Gliniana 16, 50-525 Wrocław
- Bujwida 51, 50-368 Wrocław
- Okulickiego 2A, 51-216 Wrocław
- Pautscha 4, 51-642 Wrocław
- Biegła 2, 52-115 Wrocław

## Lodówki Jadłodzielni Foodsharing Wrocław

https://www.bit.ly/mapa-jadlodzielni

- Gajowicka 96a, 53-422 Wrocław
- Swojczycka 118, 51-502 Wrocław
- Swojczycka 82, 51-503 Wrocław
- Rydygiera 25a, 50-248 Wrocław
- Hercena 13, 50-453 Wrocław
- Zagony 63, 54-614 Wrocław
- Prusa 37, 50-319 Wrocław
- Kamieńskiego 190, 51-124 Wrocław
- Hubska 8-16, 50-502 Wrocław
- Hallera 52, 53-324 Wrocław
- Wiosenna 14, 53-017 Wrocław
- Krzywoustego 286, 51-312 Wrocław
- Grabiszyńska 56, 53-504 Wrocław
- Suwalska 11 Centrum Aktywności Lokalnej, 54-104 Wrocław
- Św. Jacka, 52-050 Wrocław

