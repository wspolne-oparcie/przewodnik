# Wydawka Fundacji PRESTO

## informacje

Adres: Łukasińskiego 10, 50-438 Wrocław przedmieście oławskie

Najblisze przystanki: Komuny Paryskiej , Plac Zgody , Plac Wróblewskiego

Czynne: wtorki (czasami także środy) 10:00-13:30

Kontakt: telefon: +48 729 970 808

## opis

Darmowe wydawki jedzenia, głównie z sieci Biedronka. Dużo owoców, warzyw, wypieków i mięsa. Trochę produktów suchych. 