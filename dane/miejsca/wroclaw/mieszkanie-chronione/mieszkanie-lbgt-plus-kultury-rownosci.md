# Mieszkanie LGBT+ Kultury Równości

## informacje

Adres: Kniaziewicza 28/2, 50-455 Wrocław

Adres jest adresem biura organizacji.

Najbliższe przystanki: Kościuszki , Komuny Paryskiej , Dworzec Główny (Dworcowa)

Czynne / Dostępne: (po umówieniu się)

Kontakt telefoniczny: +48 797 721 189

Kontakt internetowy: anna.smarzynski@kulturarownosci.org

Strona internetowa: https://kulturarownosci.org/pokoj-interwencyjny/ 

## opis

Organizacja oferuje:

- bezpieczne schronienie i odpowiednie warunki do zaspokojenia podstawowych potrzeb życiowych;
- pomoc psychologiczną, prawną, zawodową;
- wsparcie asystenta w uporządkowaniu sytuacji emocjonalnej, socjalnej, życiowej i prawnej.

Podstawą przyjęcia jest rozmowa kwalifikacyjna przeprowadzona przez zespół rekrutacyjny powołany przez stowarzyszenie Kultura Równości. Osoba może zostać przyjęta do mieszkania po zaakceptowaniu regulaminu. Warunkiem jest motywacja do wyjścia z kryzysu i ustabilizowania sytuacji życiowej. Pobyt w mieszkaniu trwa do 3 miesięcy. 