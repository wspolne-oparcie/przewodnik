# Miejski Ośrodek Pomocy Społecznej Wrocław

## Adres

Miejski Ośrodek Pomocy Społecznej we Wrocławiu
53-611 Wrocław, Strzegomska 6. Otwarcie mapy
fax: 71 78 22 405
e-mail: sekretariat@mops.wroclaw.pl

## Sprawy pilne

Kontakt telefoniczny pod numerami:

- +48 717 822 300
- +48 717 822 301
- +48 717 822 302
- +48 717 822 303

Czynne od poniedziałku do piątku w godzinach 7:30 do 19:00

## Strona internetowa

https://mops.wroclaw.pl/

## Działy

### Zespół do spraw osób bezdomnych i uchodźców

Adres: Zachodnia 3, pokój 1-8, 53-643 Wrocław
Kontakt: telefon +48 717 823 580

Diagnozowanie sytuacji bytowej, zawodowej, zdrowotnej, rodzinnej i materialnej osób bezdomnych i uchodźców przebywających w obrębie miasta.
Udzielanie pomocy w postaci pracy socjalnej, świadczeń finansowych i rzeczowych osobom bezdomnym i uchodźcom.
Kierowanie do schronisk i noclegowni.