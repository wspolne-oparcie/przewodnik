# Noclegownia im. św. Brata Alberta dla mężczyzn w kryzysie bezdomności we Wrocławiu

## informacje 

Adres: Małachowskiego 15, 50-084 Wrocław Krzyki

Najbliższe przystanki: Pułaskiego , Dworzec Główny , Hubska (Dawida)

Czynne / Dostępne: całodobowo

Kontakt telefoniczny: +48 717 173 425

Kontakt internetowy: wroclaw-noclegownia@tpba.pl

Strona internetowa: https://www.bratalbert.wroclaw.pl/noclegownia-malachowskiego 

## opis

Schronienie, dostęp do kuchni (aneks), pralnia, łaźnia (czynna w godz. 9:00-16:00), świetlica z TV, pracownik socjalny, pomoc prawna, pomoc rzeczowa, terapia uzależnień, pomoc medyczna (pielęgniarka i lekarz), opieka duszpasterska, magazyn odzieży, wycieczki, środki czystości, drużyna piłkarska.

Bez skierowania, przyjmowane osoby z Wrocławia i spoza Wrocławia, które są trzeźwe oraz samoobsługowe.

Działania aktywizujące podopiecznych takie jak:

- pomoc w wyrobieniu dowodu osobistego;
- mobilizowanie podopiecznych do rejestrowania się w Urzędzie Pracy;
- pomoc w znalezieniu zatrudnienia;
- podejmowanie inicjatyw w celu otrzymania bądź wynajęcia przez podopiecznych mieszkania.