# Kuchnia charytatywna Fundacji Antoni przy Klasztorze Franciszkanów

## informacje

Adres: Kasprowicza 26, 51-349 Wrocław Karłowice

Najblisze przystanki: Pola , Broniewskiego , Syrokomli

Czynne: od poniedziałku do soboty od godziny 12.00 do wydania ostatniego posiłku

Kontakt telefoniczny: +48 713 273 593

Strona internetowa:
http://fundacjaantoni.pl/pl/dzialania/kuchnia-charytatywna/ ,
https://swantoni.org/kuchnia-charytatywna/

## opis

Ciepłe posiłki (wydawane na miejscu i na wynos) 6 dni w tygodniu. Raz w miesiącu wydawana jest żywność w ramach programu pomocy żywnościowej PEAD: mleko, mąka, makaron, ser żółty i topiony, cukier, kasza.

Kuchnia czynna jest od poniedziałku do soboty od godziny 12.00 do wydania ostatniego posiłku. Zupa wydawana jest dla osób jedzących na miejscu i na wynos. Każda osoba, co przychodzi zostaje nakarmiona. Na darmowe posiłki przychodzą głównie osoby starsze, w kryzysie bezdomności, ubogie, osoby uzależnione oraz rodziny wielodzietne. Posiłki składają się z gorącej zupy, pieczywa i herbaty. Często w miarę możliwości do posiłków dodawane są inne produkty żywnościowe.