# CARITAS Centrum Socjalne Archidiecezji Wrocławskiej

## informacje

Adres: Słowiańska 17, 50-234 Wrocław Nadodrze

Najblisze przystanki: Słowiańska , Dworzec Nadodrze , Nowowiejska

Czynne: posiłki wydawane są od poniedziałku do piątku w godz. 12.00 – 14.00. W sezonie zimowym (listopad – marzec) posiłki wydawane są dodatkowo w sobotę. W sierpniu jadłodajnia jest nieczynna.

Kontakt telefoniczny: +48 713 721 986

## opis

Talon na posiłek można odebrać w oddziale Miejskiego Ośrodka Pomocy Społecznej Wrocław przy ulicy Strzegomskiej 6.

Z posiłków wydawanych przez wrocławską jadłodajnię pierwsze korzystają osoby posiadające abonament wydany przez Miejski Ośrodek Pomocy Społecznej oraz Parafialne Zespoły Caritas.