# Kuchnia charytatywna Fundacji Sancta Familia

## informacje

Adres: Monte Cassino 64, 51-681 Wrocław Sępolno

Najblisze przystanki: Monte Cassino , Libelta , Bacciarellego

Czynne: od poniedziałku do soboty w godzinach 12:00 do 13:30

Kontakt telefoniczny: telefon +48 517 45 35 75

Kontakt internetowy: fundacja@safa.org.pl

Strona internetowa: https://sanctafamilia.pl/kuchnia-charytatywna/

## opis

Codziennie wydawane ponad 200 posiłków. Z posiłku zazwyczaj korzystają najczęściej osoby starsze, w kryzysie bezdomności, osoby w kryzysie uzależnienia i rodziny wielodzietne. Często jest to dla tych osób jedyny posiłek w ciągu dnia. Kuchnia charytatywna jest wspierana przez gminę Wrocław. Współpracuje także z Bankiem Żywności.

Przy przygotowaniu posiłków pracują zarówno stali pracownicy jak i wolontariusze. Jeżeli jakaś osoba chce wesprzeć pracę kuchni charytatywnej, to proszona jest o kontakt do fundacji mailowo lub telefonicznie. Pomoc jest potrzebna w od poniedziałku do soboty w godzinach 8.00-14.00.