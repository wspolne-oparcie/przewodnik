# Harmonogram wydawek posiłków we Wrocławiu

## poniedziałek: 

- Kuchnia charytatywna Fundacji Antoni, Kasprowicza 26: Od godziny 12.00
- Kuchnia charytatywna Fundacji Sancta Familia, Monte Cassino 64: 12:00 – 13:30
- Posiłki CARITAS Centrum Socjalne Archidiecezji Wrocławskiej, Słowiańska 17: 12.00 – 14.00
- Wydawka Fundacji WeźPomóż, Poznańska 58: 16:00 – 19:00

## wtorek:

- Kuchnia charytatywna Fundacji Antoni, Kasprowicza 26: Od godziny 12.00
- Wydawka Fundacji PRESTO, Łukasińskiego 10	: 10:00 – 13:30
- Kuchnia charytatywna Fundacji Sancta Familia, Monte Cassino 64: 12:00 – 13:30
- Posiłki CARITAS Centrum Socjalne Archidiecezji Wrocławskiej, Słowiańska 17: 12.00 – 14.00
- Wydawka Fundacji WeźPomóż, Poznańska 58: 12:00 – 16:00

## środa

- Kuchnia charytatywna Fundacji Antoni, Kasprowicza 26: Od godziny 12.00
- Kuchnia charytatywna Fundacji Sancta Familia, Monte Cassino 64: 12:00 – 13:30
- Posiłki CARITAS Centrum Socjalne Archidiecezji Wrocławskiej, Słowiańska 17: 12.00 – 14.00
- Wydawka Fundacji WeźPomóż, Poznańska 58: 12:00 – 16:00

## czwartek

- Wydawka Fundacji Z Głodu i pragnienia na rzecz kultury przetrwania, CRK przy Jagiellończyka 10cd: Od około 11:00 do około 12:30
- Kuchnia charytatywna Fundacji Antoni, Kasprowicza 26: Od godziny 12.00
- Kuchnia charytatywna Fundacji Sancta Familia, Monte Cassino 64: 12:00 – 13:30
- Posiłki CARITAS Centrum Socjalne Archidiecezji Wrocławskiej, Słowiańska 17: 12.00 – 14.00

## piątek

- Kuchnia charytatywna Fundacji Antoni, Kasprowicza 26: Od godziny 12.00
- Kuchnia charytatywna Fundacji Sancta Familia, Monte Cassino 64: 12:00 – 13:30
- Posiłki CARITAS Centrum Socjalne Archidiecezji Wrocławskiej, Słowiańska 17: 12.00 – 14.00

## sobota

- Kuchnia charytatywna Fundacji Antoni, Kasprowicza 26: Od godziny 12.00
- Kuchnia charytatywna Fundacji Sancta Familia, Monte Cassino 64: 12:00 – 13:30
- Food not bombs Wrocław, jedzenie zamiast bomb, przystanek Dworzec Nadodrze: Od 15:00

## niedziela

- Wydawka Zupa na Wolności, Plac Wolności: Od 17:00