# Inne bazy informacji pomocowych


## Zasięg krajowy - Polska


### OIK – Ośrodki Interwencji Kryzysowej w Polsce, mapa

http://www.oik.org.pl/


### Bezpłatne miejsca pomocowe dla osób w kryzysie samobójczym:

https://zwjr.pl/bezplatne-miejsca-pomocowe


### Baza miejsc pomocowych zajmujących się przeciwdziałaniem przemocy w rodzinie

https://www.niebieskalinia.pl/jak-pomagamy/baza-miejsc-pomocowych


### Bezpłatne numery pomocowe grupy Życie Jest Warte Rozmowy

https://zwjr.pl/bezplatne-numery-pomocowe


### Fundacja Tęcza Po Burzy – mapa wsparcia

https://www.teczapoburzy.com/mapa-wsparcia


## Zasięg miejski - Wrocław

### Oficjalny portal internetowy Wrocławia – infolinie i potrzebne numery

Numery telefonów i infolinie, gdzie ofiary przemocy mogą znaleźć pomoc.

https://www.wroclaw.pl/portal/przemoc-infolinia


### Miejski Ośrodek Pomocy Społecznej we Wrocławiu – placówki dla osób w kryzysie bezdomności

https://www.mops.wroclaw.pl/formy-pomocy/placowki-dla-bezdomnych
