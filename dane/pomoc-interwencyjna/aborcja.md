# Aborcja


## Telefony zaufania


### Telefon Federa - porada lub interwencja w sprawie dostępu do aborcji

Telefon: 226 359 395

Email: info@federa.org.pl 


## Zasięg światowy


### Women Help Women

https://womenhelp.org/


### Aborcja za granicą

https://aborcyjnydreamteam.pl/aborcja-za-granica/

Abortion Network Amsterdam (ANA) – abortionnetwork.amsterdam

Ciocia Basia – facebook.com/ciociabasiaberlin

Ciocia Czesia – ciociaczesia.pl

Ciocia Wienia – facebook.com/ciociawienia


## Zasięg krajowy - Polska


### Antykoncepcja dzień po, EllaOne

https://jaroslawgornicki.pl/ellaone/


### Kolektyw Dzień Po 

Ogólnopolskie wsparcie w potrzeby zastosowania antykoncepcji awaryjnej

dzienpo@riseup.net


### Lekarze Kobietom

Pomoc w dostępie do antykoncepcji awaryjnej. Dane kontaktowe do osób lekarskich przekazywane są bezpośrednio zgłaszającym się osobom w sytuacji awaryjnej.

https://lekarzekobietom.pl/

https://lekarzekobietom.pl/jestem-pacjentka/lista-lokalizacji/


### Konsultacje prawnicze bezpłatnie

Telefon: 223 070 791

Email: prawo@aborcyjnydreamteam.pl

Czynne: poniedziałki i środy, 12:00 do 18:00

Prawniczki Pro Abo 