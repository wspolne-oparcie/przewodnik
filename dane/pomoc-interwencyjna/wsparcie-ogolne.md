# Wsparcie ogólne

## Telefony zaufania


### Telefon wsparcia dla osób po stracie i w żałobie - Fundacja nagle sami

Telefon: +48 800 108 108

Czynne: od poniedziałku do piątku w godzinach 14:00 do 20:00


### Telefoniczne zgłoszenie zaginięcia - Fundacja Itaka

Telefon: +48 226 547 070

Czynne: całodobowo, 7 dni w tygodniu

http://www.zaginieni.pl


### Telefon zaufania Rzecznika Praw Dziecka

Telefon: +48 800 121 212

Czynne: całodobowo

https://800121212.pl/


### Infolinia Rzecznika Praw Obywatelskich

Telefon: +48 800 676 676

Czynne: w poniedziałek od 10.00 do 18.00, od wtorku do piątku od 8.00 do 16.00


### Telefon pomocowy i helpdesk dla mężczyzn - Instytut Przeciwdziałania Wykluczeniom

Telefon: +48 608 271 402

Czynne: czwartki, w godzinach 19:00-21:00

E-mail: biuro@fundacjaipw.org

https://www.fundacjaipw.org


### Młodzieżowy Telefon Zaufania Grupy Ponton

Telefon: +48 226 359 392

https://ponton.org.pl/telefon-zaufania/


## Zasięg krajowy - Polska


### Fundacja zaginieni

https://fundacjazaginieni.pl/



