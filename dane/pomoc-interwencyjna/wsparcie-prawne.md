# Wsparcie prawne


## Telefony zaufania


### Telefon pomocowy – infolinia prawna dla wszystkich osób

Telefon: +48 573 185 626

Czynne: w dniach poniedziałek, środa, piątek, w godzinach 17.00 do 21.00

E-mail: infoliniaprawna@federa.org.pl

Federa Fundacja Na Rzecz Kobiet i Planowania Rodziny, https://federa.org.pl/infolinia-prawna-pol-ukr


### Dolnośląski telefon antyrepresyjny Iglica

Telefon: +48 718 810 325


### Numer antyrepresyjny Szpila

Telefon: +48 722 196 139

Poza telefonem, możliwość kontaktu przez SMS, Signal, Telegram (jeśli nie jest to sprawa pilna)

https://szpila.blackblogs.org/numer-antyrepresyjny-co-jak-kiedy/


