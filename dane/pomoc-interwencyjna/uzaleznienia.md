# Uzależnienia

## Telefony zaufania

### Telefon interwencyjny dla osób nieutrzymujących abstyencji

Telefon: +48 668 351 313

Czynne: codziennie w godzinach 10:00 do 22:00

Interwencja kryzysowa, konsultacje psychologiczne, prawne.
W ramach interwencji nie jest konieczna wcześniejsza rejestracja. Interwent kryzysowy jest dostępny w tej poradni również w weekendy: sobota i niedziela w godzinach 12:00 do 15:00. Więcej szczegółów jest do znalezienia pod numerem: +48 713 070 149

Fundacja Salida we Wrocławiu, http://fundacjasalida.pl/?page_id=53


## Zasięg krajowy - Polska


### Ogólnopolski Telefon Zaufania Narkotyki – Narkomania

Telefon: +48 800 199 990

Czynne: codzienne od 16:00 do 21:00


### Ogólnopolski Telefon Zaufania - Uzależnienia behawioralne

Telefon: +48 801 889 880

Czynne: codzienne od 17:00 do 22:00 


## Zasięg miejski - Wrocław


### Fundacja Salida we Wrocławiu - pomoc w walce z uzależnieniami

Konsultacje terapeutyczne, spotkania grupowe, pomoc prawna, doradzctwo finansowe, świetlica drop-in, mobilne działania outreach - bus kursujący po Wrocławiu

http://fundacjasalida.pl


### Wrocławskie Stowarzyszenie na Rzecz Osób Wykluczonych i Zagrożonych Wykluczeniem Społecznym „Podwale Siedem”

Adres: Wszystkich Świętych 2a, 50-136 Wrocław

Telefon: +48 713 560 780

Bezpłatne: poradnia Metadonowa, poradnia medycyny podróży, punkt Szczepień, poradnia HIV/AIDS, badanie w kierunku zakażenia HIV, informacje o PrEP, poradnia chorób zakaźnych, Infolinia HCV.

http://podwale-siedem.pl/