# Wsparcie migracyjne


## Zasięg krajowy - Polska


### Fundacja Ocalenie

Pomoc osobom cudzoziemczym, uchodźczym, w tym pomoc dzieciom. Centra pomocowe w Łomży i Warszawie. Bezpłatne lekcje języka polskiego. Poszukiwanie pokoi i mieszkań. Prowadzenie warsztatów edukacyjnych antydyskryminacyjnych.

https://ocalenie.org.pl/


### No Borders Team

Oddolna grupa anarchistyczna ratująca osoby uchodźcze na granicy białoruskiej i ukraińskiej

https://nobordersteam.noblogs.org/


## Zasięg miejski - Wrocław


### NOMADA Stowarzyszenie na rzecz integracji społeczeństwa wielokulturowego - nieodpłatne poradnictwo dla osób migranckich i uchodźczych

Telefon: +48 791 576 459

Czynne: w godzinach 10.00 – 17.00

E-mail: counseling@nomada.info.pl

Istnieje również możliwość umówienia się na wizytę po wcześniejszym kontakcie telefonicznym lub mailowym.

