# Wsparcie osób różnorodności tożsamości płciowych i orientacji seksualnych

## Telefony zaufania

### Linia wsparcia - Olsztyński Marsz Równości

Telefon: +48 605 992 983

Czynne: poniedziałek 17:00-18:00


## Zasięg krajowy - Polska


### Trans-Fuzja

Konsultacje psychologiczne online: psycholog@transfuzja.org

https://www.transfuzja.org/pomoc


### Queerowy Maj

Rozmowa online z osobą psychologiczną w każdy poniedziałek w godzinach od 16 do 19. Można też zgłaszać się na wsparciowego maila: wsparcie@queerowymaj.org

https://www.facebook.com/queerowymaj


### Miłość nie wyklucza

https://mnw.org.pl/pomoc/


### Kampania przeciw homofobii

https://kph.org.pl/pomoc/pomoc-psychologiczna/

https://kph.org.pl/pomoc/pomoc-prawna/

https://kph.org.pl/pomoc/biblioteka-i-pomoc-w-badaniach-naukowych/


### Lambda Warszawa

https://lambdawarszawa.org/pomoc/


## Zasięg miejski - Wrocław


### Projekt Wsparcia LGBT+ MOPS Wrocaw

Telefon: +48 800 292 137

Miejski Ośrodek Pomocy Społecznej we Wrocławiu

https://www.mops.wroclaw.pl/aktualnosci/informacje/1860-projekt-wsparcia-lgbt


### Kultura Równości we Wrocławiu

Pomoc psychologiczna, prawna, sekspozytywna, pokój interwencyjny, mieszkanie interwencyjne.

https://kulturarownosci.org/pomoc/