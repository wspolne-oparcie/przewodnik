# Przemoc i zagrożenie życia


Ratunkowy 112


## Telefony zaufania, interwencyjne


### Telefon Federa - zagrożenie życia w trakcie bycia w ciąży

Telefon: 501 694 202

Fedara, https://federa.org.pl/aborcja-poradnik/


### Telefon dla kobiet doświadczających przemoc | Телефон Для Жінок, Які Зазнають Насильства

Telefon: +48 888 883 388

Czynne: poniedziałek-piątek, 11:00-19:00

Контакти: +48 888 88 79 88

Активний з понеділка по п’ятницю з 14:00 до 17:00

Feminoteka, https://feminoteka.pl/


### Antyprzemocowa linia pomocy

Telefon: +48 720 720 020

Czynne: od poniedziałku do piątek w godzinach 16:00 do 20:00

Sexed, https://www.sexed.pl


### Poradnia Telefoniczna „Niebieskiej Linii”

Telefon: +48 226 687 000

Czynne: codziennie w godzinach 08:00 – 20:00

Ogólnopolskie Pogotowie dla Ofiar Przemocy w Rodzinie „Niebieska Linia”, https://www.niebieskalinia.pl/jak-pomagamy/poradnia-telefoniczna 


### Telefon Zaufania dla Dzieci i Młodzieży

Telefon: 116 111

Czynne: bezpłatnie, 7 dni w tygodniu, całą dobę

Fundacja Dajemy Dzieciom Siłę, https://116111.pl/


### Telefon zaufania Rzecznika Praw Dziecka

Telefon: +48 800 121 212

Czynne: całodobowo

https://800121212.pl/ 


## Zasięg miejski - Wrocław


### Stowarzyszenie Pomocy Akson - grupa wsparcia, pomoc pedagogiczna, pomoc prawna, pomoc psychoterapeutyczna, pomoc socjalna

Telefon: 71 736 06 82

Adres: Bora Komorowskiego 31, 51-210 Wrocław

Grupa wsparcia dla osób dorosłych, które doświadczyły przemocy w rodzinie. 
Pomoc pedagogiczna, we wtorki od 15:00 do 18:00 , dla ofiar przemocy w rodzinie, głównie kobiet, matek z dziećmi.
Pomoc prawna, w poniedziałki od 15:30 do 19:30 i w czwartki od 16:00 do 18:00, dla osób pokrzywdzonych przemocą w rodzinie.
Pomoc psychoterapeutyczna, w środy od 15:00 do 20:00 i w piątki od 10:30 do 13:30, dla osób doświadczonych przemocą w rodzinie.
Pomoc socjalna, we wtorki od 17:00 do 20:00, dla ofiar przemocy w rodzinie.

Wymagane umówienie się na pierwsze spotkanie, po zadzwonieniu.

https://www.akson.org.pl/jak-pomagamy/


### Fundacja NON LICET we Wrocławiu

Pomoc osobom, które znajdują się w trudnej sytuacji materialnej i społecznej, w szczególności osobom doświadczającym przemocy domowej i kryzysu psychicznego.

https://www.nonlicet.pl