# Kryzys psychiczny


## Telefony zaufania

### Telefon zaufania w kryzysie psychologicznym, przemocy, konfliktów, chęci edukacji

Telefon: +48 226 359 295

Czynne: od poniedziałku do piątku, w godzinach 16:00 do 20:00

Wsparcia psychologiczne, porady ginekologicznych i z zakresu edukacji seksualnej.

Federa Fundacja Na Rzecz Kobiet i Planowania Rodziny, https://federa.org.pl/grafik-tz


### Telefon Zaufania dla Osób Dorosłych w Kryzysie Emocjonalnym

Telefon: 116 123

Czynne: bezpłatnie, 7 dni w tygodniu, w godzinach 14.00–22.00

Instytut Psychologii Zdrowia Polskiego Towarzystwa Psychologicznego, https://psychologia.edu.pl/kryzysy-osobiste/telefon-kryzysowy-116123.html


## Zasięg miejski - Wrocław


### Pomoc interwencyjna - Stowarzyszenie Pomocy Akson

Telefon: 71 352 94 03

Czynne: całodobowo

Adres: Gliniana 28/30, 50-525 Wrocław

Korzystanie z pomocy: 1. Zadzwonienie na telefon, 2. Umówienie się na pierwsze spotkanie w biurze stowarzyszenia

https://www.akson.org.pl/pomoc-psychologiczna-i-terapeutyczna/


### Dolnośląskie Centrum Zdrowia Psychicznego we Wrocławiu, Centrum Zdrowia Psychicznego

Pomoc dorosłym osobom, mieszkajcym w dzielnicy Wrocław Psie Pole, w rozwiązywaniu różnorodnych kryzysów i zaburzeń zdrowia psychicznego. Bezpłatnie, bez skierowania, bez kolejek.

https://www.dczp.wroclaw.pl/czp-menu/o-czp-menu