# Teleplatforma Pierwszego Kontaktu (TPK)

## Telefon +48 800 137 200

## Formularz online https://dom.mz.gov.pl/nocna-swiateczna-opieka

Strona informacyjna: https://www.gov.pl/tpk

Dostępne języki, jakie są obsługiwane: polski, ukraiński, angielski.

Osoby głuche mają możliwość umówienia się na wideoporadę lekarską, która jest tłumaczona przez profesjonalnych tłumaczy polskiego języka migowego.

### Działanie platformy

W trakcie rozmowy konsultacyjnej zostanie przeprowadzony wstępny wywiad, podczas którego pojawią się pytania o imię i nazwisko oraz numer PESEL. Następnie skontaktuje się z osobą dzwoniącą osoba pielęgniarska i w razie potrzebna osoba lekarska.

### Godziny pracy platformy

Poza godzinami pracy Podstawowej Opieki Zdrowotnej, czyli:

- od poniedziałku do piątku w godzinach od 18:00 do 8:00 następnego dnia
- w soboty i niedziele oraz inne dni ustawowo wolne od pracy, w godzinach od 8:00 do 8:00 następnego dnia

### Kiedy warto skorzystać

- w trakcie nagłego zachorowania, poza godzinami pracy POZ,
- w trakcie nagłego pogorszenia stanu zdrowia, gdy nie ma objawów sugerujących stan bezpośredniego zagrożenia życia, a zastosowane środki lub leki dostępne bez recepty nie przyniosły spodziewanej poprawy,
- gdy zachodzi obawa, że oczekiwanie na otwarcie przychodni POZ może znacząco niekorzystnie wpłynąć na stan zdrowia.

### Co można otrzymać

- poradę medyczną
- e–receptę
- e–skierowanie
- e–zwolnienie.